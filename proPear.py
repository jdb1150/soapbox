import random
import re
from time import sleep
import tweepy

#picks 2 of the 4 hashtags and returns them as a string, to be added to the end of pear tweets
def addHashtags():
    hashtags = ['#pickpears', '#pearsarebetter','#pearsvsapples','#pearfect']
    nums = set()
    while len(nums) < 2:
        nums.add(random.randrange(4))
    return ' ' + hashtags[nums.pop()] + ' ' + hashtags[nums.pop()]

def promotePears(api, interval):
    counter = 0
    tweetNum = random.randrange(10) # the number in brackets should be one more than however many tweets are in the .txt file
    tweet = ''

    with open('pearTweets.txt') as pearTweets:
        while counter < tweetNum:
            tweet = pearTweets.readline()
            counter += 1

    tweet = tweet.rstrip('\n\r') + addHashtags()
    #api.update_status(tweet)
    recentTweets = api.user_timeline(api.me().id, count = 100)
    for i in range(0, 99):
        if not recentTweets[i].retweeted:
            print(recentTweets[i].text)